# Console Application in .Net 6.0

Ein einfaches Template, was für den Einstige in C# mit .Net 6.0 verwendet werden kann.

In ihm ist ein Beispiel für eine einfache Konsolenimplementierung gegeben bei der über die Eingabe eines Radius ein Kreisumfang berechnet werden kann.\
Für das Beispiel werden Testfälle in NUnit bereitgestellt.

Die Aufgabe für diese Projekt lautet:\
Finden Sie mit Hilfe der Testfälle alle Fehler in der Funktion '*Konsolenanwendung.Geometrie.Kreis.BerechneKreisumfang(string? eingabe)*'.

# License
MIT
